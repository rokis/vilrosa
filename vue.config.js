module.exports = {
    runtimeCompiler: true,
    pwa: {
        manifestOptions: {
            icons: [
                {
                    'src': 'icons/android-icon-192x192.png',
                    'sizes': '192x192',
                    'type': 'image/png',
                },
                {
                    'src': 'icons/android-icon-512x512.png',
                    'sizes': '512x512',
                    'type': 'image/png',
                },
                {
                    'src': 'icons/favicon-16x16.png',
                    'sizes': '16x16',
                    'type': 'image/png',
                },
                {
                    'src': '/icons/favicon-32x32.png',
                    'sizes': '32x32',
                    'type': 'image/png',
                },
                {
                    'src': 'icons/favicon-96x96.png',
                    'sizes': '96x96',
                    'type': 'image/png',
                },
                {
                    "src": "icons/maskable_icon.png",
                    "sizes": "196x196",
                    "type": "image/png",
                    "purpose": "any maskable"
                }
            ]
        }
    }
};
