import Vue from 'vue'
import App from './App.vue'

import '/src/assets/scss/index.scss'
import bootstrap from 'bootstrap/dist/js/bootstrap'
import i18n from '@/plugins/i18n.js'
import FlagIcon from 'vue-flag-icon'

import router from './router'
import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {faMapMarkedAlt} from '@fortawesome/free-solid-svg-icons/faMapMarkedAlt'
import {faFileAlt} from '@fortawesome/free-solid-svg-icons/faFileAlt'
import {faMobile} from '@fortawesome/free-solid-svg-icons/faMobile'
import {faPhone} from '@fortawesome/free-solid-svg-icons/faPhone'

import Vuelidate from 'vuelidate'
import axios from 'axios'

library.add(faMapMarkedAlt, faFileAlt, faMobile, faPhone);

Vue.use(Vuelidate);

Vue.use(FlagIcon);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.config.productionTip = false;
Vue.prototype.$http = axios;
Vue.prototype.$contact_url = process.env.VUE_APP_CONTACT_URL;

function hotfixScrollSpy() {
  var dataSpyList = [].slice.call(document.querySelectorAll('[data-bs-spy="scroll"]'))
  let curScroll = getCurrentScroll();
  dataSpyList.forEach(function (dataSpyEl) {
    let offsets = bootstrap.ScrollSpy.getInstance(dataSpyEl)['_offsets'];
    for(let i = 0; i < offsets.length; i++){
      offsets[i] += curScroll;
    }
  })
}

function getCurrentScroll() {
  return window.pageYOffset || document.documentElement.scrollTop;
}

window.onload = function () {
  new bootstrap.ScrollSpy(document.body, {
    target: '#navbar-scrollspy'
  });
  hotfixScrollSpy();
  window.scrollBy(0,1);
}

new Vue({
  i18n,
  router,
  render: h => h(App),
}).$mount('#app')
