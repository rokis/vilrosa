import Vue from 'vue'
import VueRouter from 'vue-router'
import i18n from "@/plugins/i18n";

Vue.use(VueRouter);

const routes = [
    {
        path: '/:locale(lt|en)',
        component: function () {
            return import(/* webpackChunkName: "homeContainer" */ '../components/HomeContainer')
        },
        beforeEnter: (to, from, next) => {
            if(from.params.locale === to.params.locale) {
                next()
                return
            }

            const { locale } = to.params;

            const supported_locales = process.env.VUE_APP_I18N_SUPPORTED_LOCALE.split(',');
            if (!supported_locales.includes(locale)) return next('lt');

            if (i18n.locale !== locale) {
                i18n.locale = locale;
            }
            return next()
        },
        children: [
            {
                path: '',
                name: 'Home',
                component: function () {
                    return import(/* webpackChunkName: "home" */ '../components/Home.vue')
                },
                meta: {
                    title: 'Vilrosa',
                    metaTags: [
                        {
                            name: 'title',
                            content: 'Vilrosa'
                        },
                        {
                            property: 'og:title',
                            content: 'Vilrosa'
                        },
                        {
                            name: 'description',
                            content: 'We are a wholesaler of juice concentrates, NFC juices, purees, IQF, and flavours serving the Baltic states and Scandinavian market since 2014.'
                        },
                        {
                            property: 'og:description',
                            content: 'We are a wholesaler of juice concentrates, NFC juices, purees, IQF, and flavours serving the Baltic states and Scandinavian market since 2014.'
                        }
                    ]
                },
            },
            {
                path: 'juice-concentrates',
                name: 'Juice Concentrates',
                component: function () {
                    return import(/* webpackChunkName: "juiceConcentrates" */ '../views/JuiceConcentrates.vue')
                },
                meta: {
                    title: 'Vilrosa | Juice Concentrate',
                    metaTags: [
                        {
                            name: 'description',
                            content: 'Are you in need of juice concentrate? Look no further! Check out our product range and choose from the wide variety of flavours'
                        },
                        {
                            property: 'og:title',
                            content: 'Vilrosa | Juice Concentrate'
                        },
                        {
                            property: 'og:description',
                            content: 'Are you in need of juice concentrate? Look no further! Check out our product range and choose from the wide variety of flavours'
                        }
                    ]
                }
            },
            {
                path: 'juice',
                name: 'Juice',
                component: function () {
                    return import(/* webpackChunkName: "juice" */ '../views/Juice.vue')
                },
                meta: {
                    title: 'Vilrosa | Juice',
                    metaTags: [
                        {
                            name: 'description',
                            content: 'Are you in need of juice? Look no further! Check out our product range and choose from the wide variety of flavours'
                        },
                        {
                            property: 'og:title',
                            content: 'Vilrosa | Juice'
                        },
                        {
                            property: 'og:description',
                            content: 'Are you in need of juice? Look no further! Check out our product range and choose from the wide variety of flavours'
                        }
                    ]
                }
            },
            {
                path: 'puree',
                name: 'Puree',
                component: function () {
                    return import(/* webpackChunkName: "puree" */ '../views/Puree.vue')
                },
                meta: {
                    title: 'Vilrosa | Puree',
                    metaTags: [
                        {
                            name: 'description',
                            content: 'Are you in need of purees? Look no further! Check out our product range and choose from the wide variety of flavours'
                        },
                        {
                            property: 'og:title',
                            content: 'Vilrosa | Puree'
                        },
                        {
                            property: 'og:description',
                            content: 'Are you in need of purees? Look no further! Check out our product range and choose from the wide variety of flavoursIšvykos metu, aplankysite unikalų Lietuvos Venecija vadinamą kaimą, išvysite Vilhelmo kanalą ir Ventės ragą.'
                        }
                    ]
                }
            },
            {
                path: 'puree-concentrates',
                name: 'Puree Concentrates',
                component: function () {
                    return import(/* webpackChunkName: "pureeConcentrates" */ '../views/PureeConcentrates.vue')
                },
                meta: {
                    title: 'Vilrosa | Puree Concentrate',
                    metaTags: [
                        {
                            name: 'description',
                            content: 'Are you in need of puree concentrate? Look no further! Check out our product range and choose from the wide variety of flavours'
                        },
                        {
                            property: 'og:title',
                            content: 'Vilrosa | Puree Concentrate'
                        },
                        {
                            property: 'og:description',
                            content: 'Are you in need of puree concentrate? Look no further! Check out our product range and choose from the wide variety of flavours'
                        }
                    ]
                }
            },
            {
                path: 'flavours',
                name: 'Flavours',
                component: function () {
                    return import(/* webpackChunkName: "flavours" */ '../views/Flavours.vue')
                },
                meta: {
                    title: 'Vilrosa | Flavours',
                    metaTags: [
                        {
                            name: 'description',
                            content: 'Are you looking for flavours? Look no further! Check out our product range and choose from the wide variety of scents'
                        },
                        {
                            property: 'og:title',
                            content: 'Vilrosa | Flavours'
                        },
                        {
                            property: 'og:description',
                            content: 'Are you in need of flavours? Look no further! Check out our product range and choose from the wide variety of scents'
                        }
                    ]
                }
            },
            {
                path: 'organic-products',
                name: 'Organic Products',
                component: function () {
                    return import(/* webpackChunkName: "organicProducts" */ '../views/OrganicProducts.vue')
                },
                meta: {
                    title: 'Vilrosa | Organic Products',
                    metaTags: [
                        {
                            name: 'description',
                            content: 'Are you in need of organic products? Look no further! Check out our product range and choose from the wide variety of flavours'
                        },
                        {
                            property: 'og:title',
                            content: 'Sea-Safari Lietuva | Privatūs Vakarėliai'
                        },
                        {
                            property: 'og:description',
                            content: 'Are you in need of organic products? Look no further! Check out our product range and choose from the wide variety of flavours'
                        }
                    ]
                }
            },
        ]
    },
    {
        path: '*',
        redirect() {
            return process.env.VUE_APP_I18N_LOCALE;
        }
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router

router.beforeEach((to, from, next) => {
    console.log('coming from', from)
    console.log('going to', to)

    const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title);

    const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags);

    if (nearestWithTitle) document.title = nearestWithTitle.meta.title;

    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el.parentNode.removeChild(el));

    if (!nearestWithMeta) return next();

    nearestWithMeta.meta.metaTags.map(tagDef => {
        const tag = document.createElement('meta');

        Object.keys(tagDef).forEach(key => {
            tag.setAttribute(key, tagDef[key]);
        });

        tag.setAttribute('data-vue-router-controlled', '');

        return tag;
    })
        .forEach(tag => document.head.appendChild(tag));
    next();
});
